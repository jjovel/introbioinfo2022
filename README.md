# INTRODUCCION A LA BIOINFORMATICA #

(Disculpas por la falta de acentos en este documento; fue escrito desde un teclado en Ingles)

El objetivo de este curso es proporcionar los fundamentos de bioinformatico, en terminos practicos. Como estudio de caso,
se utiliza el analisis de un set de datos descritos en una de nuestra recientes publicaciones:

Lopez WR, Garcia-Jaramillo DJ, Ceballos-Aguirre N, Castaño-Zapata J, Acuña-Zornosa R, Jovel J. 2021. Transcriptional responses 
to Fusarium oxysporum f.sp. lycopersici (Sacc.) Snyder & Hansen infection in three Colombian tomato cultivars. 
BMC Plant Biol. 21(1):412. doi: 10.1186/s12870-021-03187-z.


### Algunos temas cubiertos en este curso ###

* La linea de comando de Linux
* El editor VIM
* Instalacion de programas desde la linea de comando
* Alineamiento y pseudo-alineamiento de secuencias NGS
* Procesando resultados de alineamientos
* Introduccion a programacion en R
* Analysis de expresion diferencial
* etc.

### Context ###

Este curso se esta desarrollando dentro de programa de Doctorado en Ciencias Agrarias
de la Universidad de Caldas, Manizales, Colombia.

Version 1.0. 2022.
